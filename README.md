# Wantsome - Tic Tac Toe

__Proiect realizat de: Amalia & Monica__

## I. Jocul pentru 2 concurenti umani (Amalia)

Proiectul curent reprezinta o varianta a clasicului joc __Tic Tac Toe ("X si 0")__, 
dezvoltat astfel incat sa suporte jocuri multiple pentru dimensiuni variabile
ale tablei de joc, regulile de castigare fiind adaptate la marimea boardului.

__Tehnologii folosite:__  

Pentru interfata grafica a aplicatiei s-a folosit tehnologia __JavaFX__.


__Codul aplicatiei:__

Este organizat astfel:
- Clasele __Move__ si __TicTacToeGame__ contin cod java,  si sunt strict legate de logica jocului,
- Clasa __GameVsHuman__ este clasa principala, derivata din javafx.application.Application, unde se creeaza
diverse elemente grafice, pentru interactiunea cu utilizatorul.


__Folosire:__  
- Lansarea aplicatiei are loc in metoda __main__, prin apelarea metodei abstracta __launch()__. Odata executata
aceasta, se va invoca mai departe metoda suprascrisa __start()__, prin intermediul careia se va crea un obiect de tip 
Stage, care impreuna cu setarea unui __scene__ va afisa practic fereastra jocului cu continutul acestuia.
  
- Inca de la inceputul jocului, utilizatorul este indrumat sa aleaga dimensiunea tablei de joc, iar in urma selectiei, continutul 
ferestrei se va schimba, afisandu-se astfel un nou *container* care va cuprinde tabelul alcatuit din *tiles* (obiecte - butoane).
Plasarea marcajelor se va face __alternativ__, accesand aceste butoane diferential, astfel jucatorul __"X" va marca prin click simplu__,
iar __"O" prin click dreapta__. 
 
- Tabloul jocului este completat si de alte elemente grafice, prin butoane ca: *Back* (posibilitatea de 
a reveni la pagina precedenta), *Replay* (resetarea jocului) sau *Quit* (iesirea din aplicatie), scorul fiind afisat la sfarsitul fiecarei 
partide, prin mici ferestre tip __Alert__.

---

## Jocul pentru jucator uman vs computer (Monica)

Aplicatia a fost dezvoltata pentru a construi jocul TicTacToe intre un __user uman__ si
__computer__. 

___Functionalitati:___
- posibilitatea de a juca una sau mai multe runde
- parasirea jocului in orice moment
- afisarea scorului total : indiferent de numarul de runde jucate, scorul va fi intotdeauna
 in favoarea computerului sau egalitate
- partea de User Interface e implementata in JavaFX. 

Aplicatia se lanseaza din clasa __GameVsComputer__, unde este configurata si interfata de JavaFX. 

In clasa GameVsComputer sunt apelate metode din clasele __TicTacToeGame__ si __Move__. 

Computerul face prima mutare la lansare, dupa care se poate opta pentru :
- un joc nou, Computerul mutand primul (_Computer Replay_)
- un joc nou, Human Playerul mutand primul (_Human Replay_)
- a parasi jocul (_Quit_)

Mutarea realizata de computer este calculata pe baza __algoritmului Minimax__ care simuleaza 
toate mutarile posibile pe tabla de joc, atat ale computerului cat si ale adversarului cu 
scopul de a decide _mutarea optima_ pentru computer. 
 
Se porneste de la premisa ca Human Playerul va juca optimal, alegand si el mutarea cea mai
convenabila in orice moment. 

Metoda __minimax__ e chemata recursiv pentru a se cobori in adancime pana cand sunt ocupate
toate pozitiile de pe tabla si e returnat scoringul cel mai favorabil pentru computer. 

Mutarile sunt simulate alternativ: dupa mutarea computerului se retine _valoarea maxima_ 
a metodei care evalueaza scorul iar dupa mutarea adversarului se retine _valoarea minima_
(care inseamna cel mai bun scor pentru Human Player).

In final se alege ramura pe care s-a calculat scorul cel mai mare pentru Computer si e 
aleasa mutarea care duce catre acel branch.  

![Algorithm](doc/minimax_algorithm_image.jpg)

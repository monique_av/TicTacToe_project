package wantsome.tictactoe.computer;

import java.util.Arrays;

/**
 * Main class
 */
public class TicTacToeGame {

    private final char[][] board = new char[3][3];
    private final static char[] playerMarks = {'X', '0'};
    private final static char emptyMark = ' ';

    private int computerScore = 0;
    private int humanScore = 0;

    public int getComputerScore() {
        return computerScore;
    }

    public int getHumanScore() {
        return humanScore;
    }

    public TicTacToeGame() {
        clearBoard();
    }

    public void clearBoard() {
        for (char[] row : board) {
            Arrays.fill(row, emptyMark);
        }
    }

    public void playHumanMove(Move move) {
        if (board[move.getRow()][move.getCol()] == emptyMark) {
            board[move.getRow()][move.getCol()] = playerMarks[1];
        }
    }

    public Move playComputerMove() {
        Move bestMove = findBestMove();
        board[bestMove.getRow()][bestMove.getCol()] = playerMarks[0];
        return bestMove;
    }

    public int detectWinner() {
        //check horizontal match
        for (int row = 0; row < 3; row++) {
            int winner = winnerOfLine(board[row][0], board[row][1], board[row][2]);
            if (winner != -1) {
                return winner;
            }
        }

        //check vertical match
        for (int col = 0; col < 3; col++) {
            int winner = winnerOfLine(board[0][col], board[1][col], board[2][col]);
            if (winner != -1) {
                return winner;
            }
        }

        //check 1st diagonal match
        int winner = winnerOfLine(board[0][0], board[1][1], board[2][2]);
        if (winner != -1) {
            return winner;
        }

        //check 2nd diagonal match
        winner = winnerOfLine(board[0][2], board[1][1], board[2][0]);
        if (winner != -1) {
            return winner;
        }

        if (!isMovesLeft()) {
            return 2; //tie
        }

        return -1; //no winner yet, game still playing
    }

    /**
     * Returns the index of the winner of this line (0/1), or -1 if the line is not complete
     */
    private static int winnerOfLine(int mark1, int mark2, int mark3) {
        if (mark1 != emptyMark && mark1 == mark2 && mark2 == mark3) {
            return mark1 == playerMarks[0] ? 0 : 1;
        }
        return -1;
    }

    public void updateScore(int winner) {
        if (winner == 0) {
            computerScore += 1;
        } else if (winner == 1) {
            humanScore += 1;
        }
    }


    // This function returns true if there are moves
    // remaining on the board. It returns false if
    // there are no moves left to play.
    public boolean isMovesLeft() {
        for (char[] row : board) {
            for (char c : row) {
                if (c == emptyMark) {
                    return true;
                }
            }
        }
        return false;
    }

    // This is the evaluation function
    private int evaluate() {
        int evalWinner = detectWinner();
        if (evalWinner == 0) {
            return +10;
        } else if (evalWinner == 1) {
            return -10;
        }
        return 0;
    }


    // This is the minimax function. It considers all
    // the possible ways the game can go and returns
    // the value of the board
    private int minimax(char[][] board, int depth, Boolean isMax) {

        int score = evaluate();
        //depth = 0;
        // If Maximizer has won the game
        // return his evaluated score
        if (score == 10)
            return score;

        // If Minimizer has won the game
        // return his evaluated score
        if (score == -10)
            return score;

        // If there are no more moves and
        // no winner then it is a tie
        if (!isMovesLeft())
            return 0;

        // If this is maximizer's move
        if (isMax) {
            int best = -100;

            // Traverse all cells
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    // Check if cell is empty
                    if (board[i][j] == emptyMark) {
                        // Make the move
                        board[i][j] = playerMarks[0];

                        // Call minimax recursively and choose
                        // the maximum value
                        best = Math.max(best, (minimax(board,
                                depth + 1, false) - (depth + 1)));

                        // Undo the move
                        board[i][j] = emptyMark;
                    }
                }
            }
            return best;
        }

        // If this is minimizer's move
        else {
            int best = 100;

            // Traverse all cells
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    // Check if cell is empty
                    if (board[i][j] == emptyMark) {
                        // Make the move
                        board[i][j] = playerMarks[1];

                        // Call minimax recursively and choose
                        // the minimum value
                        best = Math.min(best, (minimax(board,
                                depth + 1, true) + (depth + 1)));

                        // Undo the move
                        board[i][j] = emptyMark;
                    }
                }
            }
            return best;
        }
    }

    // This will return the best possible
    // move for the player
    private Move findBestMove() {
        int bestVal = -100;
        Move bestMove = new Move(0, 0);

        // Traverse all cells, evaluate minimax function
        // for all empty cells. And return the cell
        // with optimal value.
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                // Check if cell is empty
                if (board[i][j] == emptyMark) {
                    // Make the move
                    board[i][j] = playerMarks[0];

                    // compute evaluation function for this
                    // move.
                    int moveVal = minimax(board, 0, false);

                    // Undo the move
                    board[i][j] = emptyMark;

                    // If the value of the current move is
                    // more than the best value, then update
                    // best
                    if (moveVal > bestVal) {
                        bestMove = new Move(i, j);
                        bestVal = moveVal;
                    }
                }
            }
        }
        return bestMove;
    }
}

package wantsome.tictactoe;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import wantsome.tictactoe.computer.Move;
import wantsome.tictactoe.computer.TicTacToeGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static javafx.scene.control.Alert.AlertType.CONFIRMATION;
import static javafx.scene.control.Alert.AlertType.INFORMATION;

/**
 * @author Monica
 */
public class GameVsComputer extends Application {

    private boolean playable = true;
    private Tile[][] board = new Tile[3][3];
    private List<Combo> combos = new ArrayList<>();
    private Pane root = new Pane();
    private Button computerReplay = new Button("Computer Replay");
    private Button humanReplay = new Button("Human Replay");
    private Button quit = new Button("Quit");
    private Label message = new Label("Win by placing three in a row!");

    private TicTacToeGame game = new TicTacToeGame();


    private void createContent() {
        root.setPrefSize(805, 700);

        computerReplay.setTranslateX(615);
        computerReplay.setTranslateY(70);
        computerReplay.setPrefSize(185, 70);
        computerReplay.setFont(Font.font("Arial", FontPosture.ITALIC, 20));
        //computerReplay.setStyle("-fx-background-color: #90ee90; ");
        root.getChildren().add(computerReplay);

        humanReplay.setTranslateX(615);
        humanReplay.setTranslateY(260);
        humanReplay.setPrefSize(178, 70);
        humanReplay.setFont(Font.font("Arial", FontPosture.ITALIC, 20));
        //humanReplay.setStyle("-fx-background-color: #90ee90; ");
        root.getChildren().add(humanReplay);

        quit.setTranslateX(615);
        quit.setTranslateY(460);
        quit.setPrefSize(178, 70);
        quit.setFont(Font.font("Arial", FontPosture.ITALIC, 20));
        //quit.setStyle("-fx-background-color: #90ee90; ");
        root.getChildren().add(quit);

        message.setTranslateX(180);
        message.setTranslateY(635);
        message.setFont(Font.font("Arial", FontPosture.ITALIC, 25));
        root.getChildren().add(message);


        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Tile tile = new Tile();
                tile.setTranslateX(i * 200);
                tile.setTranslateY(j * 200);

                root.getChildren().add(tile);
                board[i][j] = tile;
            }
        }

        // horizontal
        for (int x = 0; x < 3; x++) {
            combos.add(new Combo(board[x][0], board[x][1], board[x][2]));
        }

        // vertical
        for (int y = 0; y < 3; y++) {
            combos.add(new Combo(board[0][y], board[1][y], board[2][y]));
        }

        // diagonals
        combos.add(new Combo(board[0][0], board[1][1], board[2][2]));
        combos.add(new Combo(board[2][0], board[1][1], board[0][2]));

        computerReplay.setOnAction(event -> {
            newGame();
            playComputer();
        });

        humanReplay.setOnAction(event -> {
            newGame();
        });

        quit.setOnAction(event -> {
            Alert alert = new Alert(CONFIRMATION);
            alert.setHeaderText("Are you sure you want to quit?");
            alert.setContentText("The score is: Computer " + game.getComputerScore() + " (X) Human " + game.getHumanScore() + " (O) ");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.orElse(null) == ButtonType.OK) {
                Platform.exit();
            }
        });
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        createContent();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        playComputer();
    }

    private void playComputer() {
        Move computerMove = game.playComputerMove();
        board[computerMove.getCol()][computerMove.getRow()].drawX();
        checkState();
    }

    private void checkState() {
        if (!game.isMovesLeft()) {
            Alert alert = new Alert(INFORMATION);
            alert.setHeaderText("Game ended with a tie");
            alert.setContentText("The score is: Computer " + game.getComputerScore() + " (X) Human " + game.getHumanScore() + " (O) ");
            alert.showAndWait();
            newGame();
        }

        for (Combo combo : combos) {
            if (combo.isComplete()) {
                playable = false;
                int winner = game.detectWinner();
                game.updateScore(winner);

                combo.tiles[0].setColor(Color.LIGHTGREEN);
                combo.tiles[1].setColor(Color.LIGHTGREEN);
                combo.tiles[2].setColor(Color.LIGHTGREEN);

                Alert alert = new Alert(INFORMATION);
                alert.setHeaderText("Game ended!");
                alert.setContentText("The score is: Computer " + game.getComputerScore() + " (X) Human " + game.getHumanScore() + " (O) ");
                alert.showAndWait();
            }
        }
    }

    private void newGame() {
        for (Tile[] t : board) {
            for (Tile tl : t) {
                tl.drawEmpty();
                tl.setColor(Color.AZURE);
                playable = true;
            }
        }

        game.clearBoard();
    }

    class Combo {
        private Tile[] tiles;

        public Combo(Tile... tiles) {
            this.tiles = tiles;
        }

        public boolean isComplete() {
            if (tiles[0].getValue().equals("X") || tiles[0].getValue().equals("O")) {
                return tiles[0].getValue().equals(tiles[1].getValue())
                        && tiles[1].getValue().equals(tiles[2].getValue());
            }
            return false;
        }

        @Override
        public String toString() {
            return "Combo{" +
                    "tiles=" + Arrays.toString(tiles) +
                    '}';
        }
    }

    class Tile extends StackPane {

        private Text text = new Text();
        private Rectangle border = new Rectangle(200, 200);

        @Override
        public String toString() {
            return "Tile{" +
                    "text=" + text +
                    '}';
        }

        private Tile() {

            border.setFill(Color.AZURE);
            border.setStroke(Color.BLACK);
            text.setFont(Font.font(72));
            setAlignment(Pos.CENTER);
            getChildren().addAll(border, text);

            setOnMouseClicked(event -> {

                if (event.getButton() == MouseButton.PRIMARY) {
                    if (!playable || getValue().equals("X") || getValue().equals("O")) {
                        return;
                    }

                    drawO();
                    int row = (int) getTranslateY() / 200;
                    int col = (int) getTranslateX() / 200;
                    game.playHumanMove(new Move(row, col));
                    checkState();

                    playComputer();
                }
            });

        }

        private String getValue() {
            return text.getText();
        }

        private void drawX() {
            text.setText("X");
        }

        private void drawO() {
            text.setText("O");
        }

        private void drawEmpty() {
            text.setText(" ");
        }

        private void setColor(Color color) {
            border.setFill(color);
        }
    }
}

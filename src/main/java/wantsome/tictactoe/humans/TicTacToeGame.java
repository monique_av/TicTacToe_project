package wantsome.tictactoe.humans;

import java.util.Arrays;

public class TicTacToeGame {

    private final int boardSize;
    private final int winSize;

    private final char[][] board;
    private final static char[] playerMarks = {'X', 'O'};
    private final static char emptyMark = ' ';

    private int scorePlayerX;
    private int scorePlayerO;

    public int getScorePlayerX() {
        return scorePlayerX;
    }

    public int getScorePlayerO() {
        return scorePlayerO;
    }

    public TicTacToeGame(int boardSize, int winSize) {
        this.boardSize = boardSize;
        this.winSize = winSize;
        board = new char[boardSize][boardSize];
        clearBoard();
    }

    public void playMoveX(Move move) {
        playMove(move, playerMarks[0]);
    }

    public void playMoveO(Move move) {
        playMove(move, playerMarks[1]);
    }

    private void playMove(Move move, char playerMark) {
        if (board[move.getRow()][move.getCol()] == emptyMark) {
            board[move.getRow()][move.getCol()] = playerMark;
        }
    }

    public int detectWinner() {

        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize - winSize + 1; j++) {
                if (board[i][j] != emptyMark) {
                    boolean allCellsAreTheSame = true;
                    for (int s = 0; s < winSize - 1; s++) {
                        allCellsAreTheSame = allCellsAreTheSame && (board[i][j + s] == board[i][j + s + 1]);
                    }
                    if (allCellsAreTheSame) {
                        return board[i][j] == playerMarks[0] ? 0 : 1;
                    }
                }
            }
        }

        for (int i = 0; i < boardSize - winSize + 1; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (board[i][j] != emptyMark) {
                    boolean allCellsAreTheSame = true;
                    for (int s = 0; s < winSize - 1; s++) {
                        allCellsAreTheSame = allCellsAreTheSame && (board[i + s][j] == board[i + s + 1][j]);
                    }
                    if (allCellsAreTheSame) {
                        return board[i][j] == playerMarks[0] ? 0 : 1;
                    }
                }
            }
        }

        for (int i = 0; i < boardSize - winSize + 1; i++) {
            for (int j = 0; j < boardSize - winSize + 1; j++) {
                if (board[i][j] != emptyMark) {
                    boolean allCellsAreTheSame = true;
                    for (int s = 0; s < winSize - 1; s++) {
                        allCellsAreTheSame = allCellsAreTheSame && (board[i + s][j + s] == board[i + s + 1][j + s + 1]);
                    }
                    if (allCellsAreTheSame) {
                        return board[i][j] == playerMarks[0] ? 0 : 1;
                    }
                }
            }
        }

        for (int i = 0; i < boardSize - winSize + 1; i++) {
            for (int j = winSize - 1; j < boardSize; j++) {
                if (board[i][j] != emptyMark) {
                    boolean allCellsAreTheSame = true;
                    for (int s = 0; s < winSize - 1; s++) {
                        allCellsAreTheSame = allCellsAreTheSame && (board[i + s][j - s] == board[i + s + 1][j - s - 1]);
                    }
                    if (allCellsAreTheSame) {
                        return board[i][j] == playerMarks[0] ? 0 : 1;
                    }
                }
            }
        }

        if (!isMovesLeft()) {
            return 2; //tie
        }

        return -1; //no winner yet, game still playing
    }

    public void updateScore(int winner) {
        if (winner == 0) {
            scorePlayerX += 1;
        } else if (winner == 1) {
            scorePlayerO += 1;
        }
    }

    public boolean isMovesLeft() {
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (board[i][j] == emptyMark) {
                    return true;
                }
            }
        }
        return false;
    }

    public void clearBoard() {
        for (char[] row : board) {
            Arrays.fill(row, emptyMark);
        }
    }
}
